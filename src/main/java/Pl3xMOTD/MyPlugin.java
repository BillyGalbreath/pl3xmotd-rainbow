package Pl3xMOTD;

import net.pl3x.pl3xlibs.Logger;
import net.pl3x.pl3xlibs.Pl3xLibs;
import net.pl3x.pl3xlibs.configuration.BaseConfig;
import net.pl3x.pl3xmotd.ServerStatus;
import net.pl3x.pl3xmotd.TextFile;
import net.pl3x.pl3xmotd.runnables.StartMetrics;
import net.pl3x.pl3xmotd.runnables.StatusRotate;
import PluginReference.MC_Server;
import PluginReference.PluginBase;
import PluginReference.PluginInfo;

public class MyPlugin extends PluginBase {
	private MC_Server server;
	private BaseConfig config;
	private Logger logger;
	private ServerStatus serverStatus = null;

	public PluginInfo getPluginInfo() {
		PluginInfo info = new PluginInfo();
		info.name = "Pl3xMOTD";
		info.version = "0.2";
		info.description = "Server status (MOTD) and icon rotator";
		return info;
	}

	@Override
	public void onStartup(MC_Server argServer) {
		server = argServer;
		init();
		Pl3xLibs.getLogger(getPluginInfo()).info(getPluginInfo().name + " v" + getPluginInfo().version + " by BillyGalbreath is now enabled.");
	}

	@Override
	public void onShutdown() {
		disable();
		Pl3xLibs.getLogger(getPluginInfo()).info("Plugin disabled.");
	}

	private void init() {
		getConfig();
		TextFile.copyFileFromJarToDisk(this, "server-status.txt");
		serverStatus = new ServerStatus(this);
		Pl3xLibs.getScheduler().scheduleRepeatingTask(getPluginInfo().name, new StatusRotate(this), 0, getConfig().getInteger("status-rotate-delay", 5) * 20);
		Pl3xLibs.getScheduler().scheduleTask(getPluginInfo().name, new StartMetrics(this), 100);
	}

	private void disable() {
		Pl3xLibs.getScheduler().cancelAllRunnables(getPluginInfo().name);
		config = null;
		logger = null;
		serverStatus = null;
	}

	public void reload() {
		disable();
		init();
	}

	public void debug(String msg) {
		if (getConfig().getBoolean("debug-mode", false)) {
			getLogger().debug(msg);
		}
	}

	public ServerStatus getServerStatus() {
		return serverStatus;
	}

	public MC_Server getServer() {
		return server;
	}

	public BaseConfig getConfig() {
		if (config == null) {
			config = new BaseConfig(MyPlugin.class, getPluginInfo(), "", "config.ini");
			config.load();
		}
		return config;
	}

	public Logger getLogger() {
		if (logger == null) {
			logger = new Logger(getPluginInfo());
		}
		return logger;
	}
}
