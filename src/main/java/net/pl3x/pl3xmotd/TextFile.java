package net.pl3x.pl3xmotd;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import net.pl3x.pl3xlibs.BetterPluginInfo;
import Pl3xMOTD.MyPlugin;

public class TextFile {
	public static List<String> getLineByLine(MyPlugin plugin, String fileName) {
		List<String> list = new ArrayList<String>();
		try {
			File file = new File(new BetterPluginInfo(plugin.getPluginInfo()).getPluginDirectory(), fileName);
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				if (line == null || line.isEmpty() || line.equals("")) {
					continue;
				}
				list.add(line);
			}
			fileReader.close();
		} catch (IOException e) {
			// error reading file, do not return partial data
			list.clear();
		}
		return list;
	}

	public static void copyFileFromJarToDisk(MyPlugin plugin, String fileName) {
		copyFileFromJarToDisk(plugin, fileName, false);
	}

	public static void copyFileFromJarToDisk(MyPlugin plugin, String fileName, boolean overwrite) {
		File file = new File(new BetterPluginInfo(plugin.getPluginInfo()).getPluginDirectory(), fileName);
		if (!overwrite && file.exists()) {
			return;
		}
		InputStream stream = MyPlugin.class.getResourceAsStream("/" + fileName);
		if (stream == null) {
			return;
		}
		OutputStream resStreamOut = null;
		int readBytes;
		byte[] buffer = new byte[4096];
		try {
			resStreamOut = new FileOutputStream(file);
			while ((readBytes = stream.read(buffer)) > 0) {
				resStreamOut.write(buffer, 0, readBytes);
			}
			stream.close();
			resStreamOut.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
