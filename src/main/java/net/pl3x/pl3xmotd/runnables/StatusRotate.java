package net.pl3x.pl3xmotd.runnables;

import net.pl3x.pl3xlibs.scheduler.Pl3xRunnable;
import Pl3xMOTD.MyPlugin;

public class StatusRotate extends Pl3xRunnable {
	private MyPlugin plugin;

	public StatusRotate(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public void run() {
		plugin.getServerStatus().rotateMOTD();
		plugin.getServerStatus().rotateIcon();
	}
}
