package net.pl3x.pl3xmotd;

import java.io.File;
import java.io.FilenameFilter;
import java.util.List;
import java.util.Random;

import net.pl3x.pl3xlibs.BetterPluginInfo;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xMOTD.MyPlugin;

public class ServerStatus {
	private MyPlugin plugin;

	public ServerStatus(MyPlugin plugin) {
		this.plugin = plugin;
	}

	public void rotateMOTD() {
		List<String> status = TextFile.getLineByLine(plugin, "server-status.txt");
		if (status.size() == 0) {
			return;
		}
		int statuschoice = new Random().nextInt(status.size());
		plugin.getServer().setServerMOTD(Pl3xLibs.colorize(replaceVars(status.get(statuschoice).replace("\\n", "\n"))));
	}

	public void rotateIcon() {
		File dir = new File(new BetterPluginInfo(plugin.getPluginInfo()).getPluginDirectory(), "server-icon");
		if (!dir.exists()) {
			dir.mkdirs();
			return;
		}
		File[] iconFiles = dir.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".png");
			}
		});
		if (iconFiles.length < 1) {
			return;
		}
		int iconchoice = new Random().nextInt(iconFiles.length);
		try {
			plugin.getServer().setServerIconFilename(iconFiles[iconchoice].getPath());
		} catch (Exception e) {
			plugin.getLogger().error("Problem loading custom server-icon: &7" + e.getLocalizedMessage());
		}
	}

	private String replaceVars(String status) {
		return status.replace("{rainbow-version}", Double.toString(plugin.getServer().getRainbowVersion())).replace("{server-version}", "1.8.0");
	}
}
