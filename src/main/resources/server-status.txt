&7This is a long server status that will wrap around to the 2nd line to utilize all the available status space given.
&7This is a shorter MOTD\n&aThat uses line-break character for 2nd line.
&5Color &2codes &3are &4supported!\n&aWhich &badd &cflavor &dto &eyour &9server statuses!
&aRunning RainbowMod v&e{rainbow-version} &a(&e{server-version}&a)
